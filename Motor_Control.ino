int p12 = 12;
int p11 = 11;
int p10 = 10;
int p9 = 9;
int p8 = 8;
int p7 = 7;
int p6 = 6;
int p5 = 5;
int p4 = 4;
int p3 = 3;
int p2 = 2;
int p1 = 1;

void setup() {
  pinMode(p12, OUTPUT);
  pinMode(p11, OUTPUT);
  pinMode(p10, OUTPUT);
  pinMode(p9, OUTPUT);
  pinMode(p8, OUTPUT);
  pinMode(p7, OUTPUT);
  pinMode(p6, OUTPUT);
  pinMode(p5, OUTPUT);
  pinMode(p4, OUTPUT);
  pinMode(p3, OUTPUT);
  pinMode(p2, OUTPUT);
  pinMode(p1, OUTPUT);

}

void loop() {
  if(digitalRead(13) == HIGH && digitalRead(0) == LOW){
    digitalWrite(p12, HIGH);
    digitalWrite(p11, LOW);
    digitalWrite(p10, HIGH);
    digitalWrite(p9, HIGH);
    digitalWrite(p8, LOW);
    digitalWrite(p7, HIGH);
    digitalWrite(p6, HIGH);
    digitalWrite(p5, LOW);
    digitalWrite(p4, HIGH);
    digitalWrite(p3, HIGH);
    digitalWrite(p2, LOW);
    digitalWrite(p1, HIGH);
  }
  else{
    digitalWrite(p12, LOW);
    digitalWrite(p11, LOW);
    digitalWrite(p10, LOW);
    digitalWrite(p9, LOW);
    digitalWrite(p8, LOW);
    digitalWrite(p7, LOW);
    digitalWrite(p6, LOW);
    digitalWrite(p5, LOW);
    digitalWrite(p4, LOW);
    digitalWrite(p3, LOW);
    digitalWrite(p2, LOW);
    digitalWrite(p1, LOW);
  }
}