import RPi.GPIO as GPIO
import time
import math
import socket
import can

GPIO.setmode (GPIO.BOARD)

TRIG = 7 #RPi pin 7
ECHO = 12 #RPi pin 12

GPIO.setup(TRIG, GPIO.OUT)
GPIO.output(TRIG,0)

GPIO.setup(ECHO, GPIO.IN)

time.sleep(0.1)


############can setup
#message value sent over can
motor_control =  can.Message(arbitration_id=0x7de, data=[0, 0, 0, 0, 0, 0x4F, 0x46, 0x46], extended_id=False)

#can bus setup
bustype = 'socketcan_native'
channel = 'can0'    #selecting can channel 0 or 1

bus = can.interface.Bus(channel=channel, bustype=bustype) #creating a bus instance for ch 0 and socketcan should be able to rx and tx
#################



for x in range(0,10): #how long will it run
    while x > 8:
        print("Measeurement ...")
        GPIO.output(TRIG,1)
        time.sleep(1)
        GPIO.output(TRIG,0)

        while GPIO.input (ECHO) == 0:
            pass
        start = time.time()

        while GPIO.input(ECHO) == 1:
            pass
        stop = time.time()

        x = ((stop - start) * 17000 )
        print (math.ceil(x), ("cm"))

print("tx: ", motor_control)
bus.send(motor_control)
GPIO.cleanup()
