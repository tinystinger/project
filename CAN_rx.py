import socket
import RPi.GPIO as GPIO
import time
import can

motor_off =  14 # Broadcom pin 14 (PI pin 8)

#pin setup
GPIO.setmode(GPIO.BCM)
GPIO.setup(motor_off, GPIO.OUT)

# Initial state for wall and IMU output:
GPIO.output(motor_off, GPIO.LOW)

#message value sent over can
motor_control =  can.Message(arbitration_id=0x7de, data=[0, 0, 0, 0, 0, 0x4F, 0x46, 0x46], extended_id=False)

#can bus setup
bustype = 'socketcan_native'  
channel = 'can0'    #selecting can channel 0 or 1

bus = can.interface.Bus(channel=channel, bustype=bustype) #creating a bus instance for ch 0 and socketcan should be able to rx and tx

try:
	#monitor can bus and output warnings to plax board
	while 1:
		msg = bus.recv() #reads can bus network looking for a message to turn off motors
		print("message received ", msg)
		#if the sensor picks up a wall within specified distance, sends a signal to turn motors. Also if the vehicle tips over, motors turn off
		if  msg:
			GPIO.output(motor_off, GPIO.HIGH)
			print("motors turning off")
		else :
			GPIO.output(motor_off, GPIO.LOW)
		
except KeyboardInterrupt: # if ctrl+c is pressed, exit and clean
	GPIO.cleanup() #cleanup all GPIO
